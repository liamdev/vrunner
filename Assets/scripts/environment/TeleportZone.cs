﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Collider))]
public class TeleportZone : MonoBehaviour{
	
	public GameObject destination;
	
	private ToggleScript fadeIn;
	private ToggleScript fadeOut;
	
	private bool triggered;
	private bool fadedOut;
	
	private Transform positionTransform;
	private Transform bodyRotationTransform;
	private Transform headRotationTransform;
	
	public void Start(){
		positionTransform = GameObject.Find("Player").transform;
		bodyRotationTransform = GameObject.Find("Body").transform;
		headRotationTransform = GameObject.Find("PlayerHead").transform;
		
		fadeIn = GameObject.Find("QuickFadeCameraIn").GetComponent<ToggleScript>();
		fadeOut = GameObject.Find("QuickFadeCameraOut").GetComponent<ToggleScript>();
	}
	
	public void teleport(){
		if(destination == null){
			Debug.LogWarning("Teleport zone triggered, but no destination has been set.");
			return;
		}
		
		bodyRotationTransform.eulerAngles = destination.transform.eulerAngles;
		headRotationTransform.eulerAngles = destination.transform.eulerAngles;
		positionTransform.gameObject.rigidbody.velocity = new Vector3(0, 0, 0);	
		positionTransform.position = destination.transform.position;
	}
	
	public void OnTriggerEnter(Collider collider){
		triggered = true;
		fadedOut = false;
		
		if(fadeOut != null)
			fadeOut.enable();
	}
	
	public void Update(){
		if(!triggered)
			return;
		
		if((fadeOut == null || (fadeOut.isDisabled()) && !fadedOut)){
			fadedOut = true;
			teleport();
			if(fadeOut != null)
				fadeIn.enable();
		}
		
		if(fadeIn == null || (fadedOut && fadeIn.isDisabled()))
			triggered = false;
	}
}
