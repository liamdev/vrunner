﻿using UnityEngine;
using System.Collections;
using System.Text;

[RequireComponent (typeof (TextMesh))]
public class TextType : ToggleScript{
	
	// User-defined variables, allow definition of the parsing language/parameters
	
	public string fullText;					// Desired output
	public float autoTextCharacterDelay;	// Time between character output in auto text mode
	public string autoTextToggleCharacter;	// Character used to toggle auto text mode on/off
	public string clearTextCharacter;		// Character used to clear the current output
	public string delayTextCharacter;		// Character used to invoke a pause in the output
	public float delayCharacterDuration;	// Duration of the pause caused by the delay character
	public string disableCharacter;			// Character used to disable the text output, until re-enabled
	public string linesUpCharacter;			// Character used to move the output up by a line.
	public string centerTextCharacter;		// Character used to change alignment to centered.
	public float averageTypeSpeed;			// 'User' typing speed
	public int typeSpeedJitterMS;			// Typing speed jitter, mimics real typing more effectively
	
	private StringBuilder sb;
	
	private float[] charRate;
	
	private int currentChar;
	private float elapsedTime;
	private int linesUp;
	
	private TextMesh textMesh;
	
	private AudioSource audioSource;
	public AudioClip[] keyPressSounds;
	
	private enum TextMode {TYPED_TEXT, AUTO_TEXT};
	private TextMode mode;
	
	void Start(){
		sb = new StringBuilder();
		
		mode = TextMode.TYPED_TEXT;
		charRate = new float[fullText.Length];
		
		for(int i = 0; i < fullText.Length; ++i){
			// Switch the text mode if encountering a switch character.
			if(fullText[i] == autoTextToggleCharacter[0])
				flipMode();
			
			// Character output rate for human typing is randomised, automatic is set to a fixed delay.
			charRate[i] = (mode == TextMode.TYPED_TEXT) ?
								averageTypeSpeed
										+ ((Random.Range(0, typeSpeedJitterMS)
										- (typeSpeedJitterMS / 2)) / 100f) :
								autoTextCharacterDelay;
		}
		mode = TextMode.TYPED_TEXT;
		
		currentChar = 0;
		elapsedTime = 0;
		linesUp		= 0;
		
		textMesh = gameObject.GetComponent<TextMesh>();
		if(textMesh == null)
			Debug.LogError("No text mesh");
		
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	void flipMode(){
		mode = (mode == TextMode.TYPED_TEXT) ? TextMode.AUTO_TEXT : TextMode.TYPED_TEXT;
	}
	
	void Update(){
		if(isDisabled() || currentChar >= fullText.Length)
			return;
		
		elapsedTime += Time.deltaTime;
		if(elapsedTime > charRate[currentChar]){
			if(fullText[currentChar] == autoTextToggleCharacter[0]){ // Switch to/from auto-text mode
				flipMode();
			} else if(fullText[currentChar] == clearTextCharacter[0]){ // Clear text output
				sb.Remove(0, sb.Length);
				textMesh.transform.Translate(0, linesUp * -textMesh.lineSpacing, 0);
				linesUp = 0;
			} else if(fullText[currentChar] == delayTextCharacter[0]){ // Delay text output
				elapsedTime -= (delayCharacterDuration - charRate[currentChar]);
			} else if(fullText[currentChar] == disableCharacter[0]){
				disable();
			} else if(fullText[currentChar] == linesUpCharacter[0]){
				textMesh.transform.Translate(0, textMesh.lineSpacing * gameObject.transform.localScale.y, 0);
				++linesUp;
			} else if(fullText[currentChar] == centerTextCharacter[0]){
				textMesh.alignment = TextAlignment.Center;
			} else { // Character output
				if(mode == TextMode.TYPED_TEXT) // Only play key press sound if in 'typing' mode.
					audioSource.PlayOneShot(keyPressSounds[Random.Range(0, keyPressSounds.Length)]);
				sb.Append(fullText[currentChar]);
				elapsedTime -= charRate[currentChar];
			}
		
			++currentChar;
			if(currentChar >= fullText.Length)
				disable();
		}
		
		textMesh.text = sb.ToString();
	}
}
