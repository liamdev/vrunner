﻿using UnityEngine;
using System.Collections;

public class RiddleCompletion : ToggleScript{
	
	public void Update(){
		if(isDisabled())
			return;
		
		GlobalVars.inLevel = false;
		
		HoverMenuManager menuManager = GameObject.Find("HoverMenuManager").GetComponent<HoverMenuManager>();
		menuManager.hideRiddle();
		
		disable();
	}
	
}
