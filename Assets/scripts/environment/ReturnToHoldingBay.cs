﻿using UnityEngine;
using System.Collections;

public class ReturnToHoldingBay : ToggleScript{
	
	private GameObject player;
	private GameObject playerBody;
	private GameObject playerHead;
	
	private HoverMenuManager menuManager;
	
	public void Start(){
		player = GameObject.Find("Player");
		playerBody = GameObject.Find("PlayerBody");
		playerHead = GameObject.Find("PlayerHead");
		
		menuManager = GameObject.Find("HoverMenuManager").GetComponent<HoverMenuManager>();
	}
	
	public void Update(){
		if(isDisabled())
			return;
		
		ToggleScript unloader = GameObject.Find("LevelUnloader").GetComponent<LevelUnloader>();
		if(unloader != null)
			unloader.enable();
		
		player.transform.position = new Vector3(0, 0, 0);
		player.transform.localEulerAngles = new Vector3(0, 0, 0);
		playerBody.transform.localEulerAngles = new Vector3(0, 0, 0);
		playerHead.transform.localEulerAngles = new Vector3(0, 0, 0);
		
		menuManager.hideMenu();
		GlobalVars.inLevel = false;
		
		disable();
	}
	
}
