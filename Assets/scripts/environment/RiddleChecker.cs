﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(TextMesh))]
public class RiddleChecker : MonoBehaviour {
	
	private string[] validAnswers;
	
	private TextMesh textMesh;
	private int lastLength;
	private bool correct;
	
	public void Start(){
		textMesh = gameObject.GetComponent<TextMesh>();
		lastLength = 0;
		correct = false;
	}
	
	public void setAnswers(string[] answers){
		this.validAnswers = answers;
		
		for(int i = 0; i < validAnswers.Length; ++i)
			validAnswers[i] = validAnswers[i].ToUpper();
		
		lastLength = 0;
		correct = false;
	}
	
	public bool isCorrect(){
		return correct;
	}
	
	public void Update(){
		if(textMesh.text.Length == lastLength)
			return;
		
		lastLength = textMesh.text.Length;
		
		foreach(string answerVariation in validAnswers){
			if(textMesh.text.ToUpper() == answerVariation){
				correct = true;
				return;
			}
		}
	}
	
}
