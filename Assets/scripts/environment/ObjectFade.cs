﻿using UnityEngine;
using System.Collections;

public class ObjectFade : ToggleScript{
	
	public enum FadeType {FADE_IN, FADE_OUT};
	
	public GameObject target;
	public float fadeDuration;
	public FadeType type;
	
	private Renderer targetRenderer;
	private float elapsedTime;
	
	void Start(){
		elapsedTime = 0;
		targetRenderer = target.renderer;
	}
	
	public int getStartAlpha(){
		return (type == FadeType.FADE_IN) ? 0 : 1;
	}
	
	public override void enable(){
		base.enable();
		
		if(!target.activeSelf && type == FadeType.FADE_OUT)
			target.SetActive(false);
		else
			target.SetActive(true);
		
		if(targetRenderer == null)
			targetRenderer = target.renderer;
		
		Color matCol = targetRenderer.material.color;
		float startAlpha = getStartAlpha();
		targetRenderer.material.color = new Color(matCol.r, matCol.g, matCol.b, startAlpha);
		
		elapsedTime = 0;
		
		if(fadeDuration == 0){
			targetRenderer.material.color = new Color(matCol.r, matCol.g, matCol.b, 1 - startAlpha);
			base.disable();		
		}
	}
	
	void Update(){
		if(isDisabled() || elapsedTime >= fadeDuration)
			return;
		
		elapsedTime += Time.deltaTime;
		
		Color col = targetRenderer.material.color;
		float alpha = (type == FadeType.FADE_IN) ? (elapsedTime / fadeDuration) :
												1 - (elapsedTime / fadeDuration);
		
		targetRenderer.material.color = new Color(col.r, col.g, col.b, alpha);
		
		if(elapsedTime >= fadeDuration){
			disable();
			if(type == FadeType.FADE_OUT)
				target.SetActive(false);
		}
	}
}
