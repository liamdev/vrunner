﻿using UnityEngine;
using System.Collections;

public class TooltipTriggerCustomText : MonoBehaviour{
	
	public string tooltipText;
	public int fontSize;
	public Color fontColour;
	
	private HoverMenuManager menuManager;
	
	public void Start(){
		menuManager = GameObject.Find("HoverMenuManager").GetComponent<HoverMenuManager>();
	}
	
	public void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag != "Player")
			return;
		
		menuManager.showTooltip(tooltipText, fontSize, fontColour);
	}
	
	public void OnTriggerExit(Collider collider){
		if(collider.gameObject.tag != "Player")
			return;
		
		menuManager.hideTooltip();
	}
	
}
