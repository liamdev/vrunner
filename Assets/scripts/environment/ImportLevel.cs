﻿using UnityEngine;
using System.Collections;

public class ImportLevel : ToggleScript{
	
	public string levelName;
	
	private AsyncOperation loadOperation;
	private ThreadPriority prevPriority;
	
	public override void enable(){
		base.enable();
		
		prevPriority = Application.backgroundLoadingPriority;
		Application.backgroundLoadingPriority = ThreadPriority.Low;
	}
	
	public override void disable(){
		base.disable();
		
		loadOperation = null;
		Application.backgroundLoadingPriority = prevPriority;
	}
	
	void Update(){
		if(isDisabled())
			return;
		
		if(loadOperation == null)
			loadOperation = Application.LoadLevelAdditiveAsync(levelName);
		else if(loadOperation.isDone){
			GlobalVars.inLevel = true;
			disable();
		}
	}
}
