﻿using UnityEngine;
using System.Collections;

public class TooltipTrigger : MonoBehaviour{
	
	public string tooltipText;
	
	private HoverMenuManager menuManager;
	
	public void Start(){
		menuManager = GameObject.Find("HoverMenuManager").GetComponent<HoverMenuManager>();
	}
	
	public void OnTriggerEnter(Collider collider){
		if(collider.gameObject.tag != "Player")
			return;
		
		menuManager.showTooltip(tooltipText, 30, Color.white);
	}
	
	public void OnTriggerExit(Collider collider){
		if(collider.gameObject.tag != "Player")
			return;
		
		menuManager.hideTooltip();
	}
	
}
