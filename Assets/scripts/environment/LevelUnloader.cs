﻿using UnityEngine;
using System.Collections;

public class LevelUnloader : ToggleScript{
	
	public GameObject[] objectsToUnload;
	
	public void Update(){
		if(isDisabled())
			return;
		
		foreach(GameObject obj in objectsToUnload)
			Destroy(obj);
		
		disable();
	}
	
}
