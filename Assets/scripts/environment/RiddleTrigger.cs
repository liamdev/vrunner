﻿using UnityEngine;
using System.Collections;

public class RiddleTrigger : MonoBehaviour {

	public string riddleText;
	public string[] validAnswers;
	public ToggleScript correctResponse;
	
	private HoverMenuManager menuManager;
	private RiddleChecker checker;
	
	private bool alreadyGuessed;
	
	public void Start(){
		menuManager = GameObject.Find("HoverMenuManager").GetComponent<HoverMenuManager>();
		alreadyGuessed = false;
	}
	
	public void OnTriggerStay(Collider collider){
		if(collider.gameObject.tag != "Player")
			return;
		
		if(Input.GetKeyDown(KeyCode.E) && !GlobalVars.menuOpen && !alreadyGuessed){
			menuManager.showRiddle(riddleText);
			checker = GameObject.Find("AnswerArea").GetComponent<RiddleChecker>();
			checker.setAnswers(validAnswers);
		}
		
		if(checker != null && checker.isCorrect() && !alreadyGuessed){
			correctResponse.enable();
			alreadyGuessed = true;
		}
	}
	
	public void OnTriggerExit(Collider collider){
		if(collider.gameObject.tag != "Player")
			return;
		
		menuManager.hideRiddle();
	}
}
