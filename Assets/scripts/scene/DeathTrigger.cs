﻿using UnityEngine;
using System.Collections;

public class DeathTrigger : MonoBehaviour {
	
	private ToggleScript returnScript;
	
	public void Start(){
		returnScript = GameObject.Find("ReturnToHoldingBay").GetComponent<ToggleScript>();	
	}
	
	public void OnTriggerEnter(Collider collider){
		if(returnScript != null && returnScript.isDisabled())
			returnScript.enable();
	}
}
