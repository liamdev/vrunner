﻿using UnityEngine;
using System.Collections;

public class MegaVirusMovement : ToggleScript{
	
	public MovementStep[] steps;
	
	private int stepIndex;
	
	public override void enable(){
		base.enable();
		
		stepIndex = 0;
		nextMoveStep();
	}
	
	public void nextMoveStep(){
		if(stepIndex >= steps.Length)
			return;
		
		MovementStep step = steps[stepIndex];
		Vector4 data = step.data;
		GameObject target = (step.target == null) ? gameObject : step.target;
		
		switch(step.type){
		case MovementStep.StepType.MOVE:
			if(step.concurrent){
				iTween.MoveTo(target, iTween.Hash(
					"position", new Vector3(data.x, data.y, data.z),
					"time", data.w,
					"easetype", iTween.EaseType.linear
					));
			} else {
				iTween.MoveTo(target, iTween.Hash(
					"position", new Vector3(data.x, data.y, data.z),
					"time", data.w,
					"easetype", iTween.EaseType.linear,
					"oncomplete", "nextMoveStep",
					"oncompletetarget", gameObject
					));
			}
			break;
		case MovementStep.StepType.SCALE:
			if(step.concurrent){
				iTween.ScaleTo(target, iTween.Hash(
					"scale", new Vector3(data.x, data.y, data.z),
					"time, data.w," +
					"easetype", iTween.EaseType.linear
					));
			} else {
				iTween.ScaleTo(target, iTween.Hash(
					"scale", new Vector3(data.x, data.y, data.z),
					"time, data.w," +
					"easetype", iTween.EaseType.linear,
					"oncomplete", "nextMoveStep",
					"oncompletetarget", gameObject
					));
			}
			break;
		default:
			break;
		}
		
		++stepIndex;
		
		if(step.concurrent)
			nextMoveStep();
	}
	
	
	[System.Serializable]
	public class MovementStep{
		public enum StepType {MOVE, SCALE, WAIT};
		
		public StepType type;
		public GameObject target;
		public bool concurrent;
		public Vector4 data;
	}
}
