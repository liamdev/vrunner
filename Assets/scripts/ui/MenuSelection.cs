﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(AudioClip))]
public class MenuSelection : ToggleScript {
	
	public GameObject rayEmitter;
	public GameObject cursorLeft;
	public GameObject cursorRight;
	public LayerMask layerMask;
	
	public AudioClip menuHoverSound;
	public AudioClip menuSelectSound;
	
	private RaycastHit hitInfo;
	private GameObject target;
	private GameObject prevTarget;
	
	private Dictionary<int, MenuButtonPress> buttonDictionary;
	
	void Start(){
		hitInfo = new RaycastHit();
		buttonDictionary = new Dictionary<int, MenuButtonPress>();
	}
	
	public override void enable(){
		base.enable();
		
		cursorLeft.SetActive(true);
		cursorRight.SetActive(true);
		
		if(GlobalVars.inLevel){
			cursorLeft.transform.localPosition = new Vector3(0, 0, 2.0f);
			cursorRight.transform.localPosition = new Vector3(-0.28f, 0, 2.0f);
			cursorLeft.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
			cursorRight.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
		} else {
			cursorLeft.transform.localPosition = new Vector3(0, 0, 3.5f);
			cursorRight.transform.localPosition = new Vector3(-0.06f, 0, 3.5f);
			cursorLeft.transform.localScale = new Vector3(0.02f, 0.01f, 0.02f);
			cursorRight.transform.localScale = new Vector3(0.02f, 0.01f, 0.02f);
		}
	}
	
	public override void disable(){
		base.disable();
		cursorLeft.SetActive(false);
		cursorRight.SetActive(false);
	}
	
	void Update(){
		if(isDisabled())
			return;
		
		if(Physics.Raycast(rayEmitter.transform.position, rayEmitter.transform.forward, out hitInfo, 10f, layerMask.value)){
			if(hitInfo.collider.gameObject.tag == "Button"){
				target = hitInfo.collider.gameObject;
			} else{
				target = null;
			}
		} else {
			target = null;
		}
		
		if(target == null){
			if(target != prevTarget){
				MenuButtonPress pressHandler = buttonDictionary[prevTarget.GetInstanceID()];
				pressHandler.endButtonHover();
			}
		} else {
			if(!buttonDictionary.ContainsKey(target.GetInstanceID())){
				buttonDictionary.Add(target.GetInstanceID(), target.GetComponent<MenuButtonPress>());
			}
			
			MenuButtonPress pressHandler = buttonDictionary[target.GetInstanceID()];
			if(target != prevTarget){
				pressHandler.beginButtonHover();
				gameObject.audio.PlayOneShot(menuHoverSound);
			}
			
			// Check if the player is pressing the button
			if(Input.GetKeyDown(KeyCode.Space)){
				if(pressHandler != null){
					gameObject.audio.PlayOneShot(menuSelectSound);
					pressHandler.pressButton();
				} else {
					Debug.LogWarning("Button \"" + target.name + "\" was pressed, but has no attached button press handler.");
				}
			}
		}
		
		prevTarget = target;
	}
}
