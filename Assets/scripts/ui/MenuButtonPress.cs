﻿using UnityEngine;
using System.Collections;

public abstract class MenuButtonPress : MonoBehaviour {
	
	private Renderer buttonRenderer;
	private Color baseColour;
	private Color brightColour;
	
	public void Start(){
		buttonRenderer = gameObject.renderer;
		
		Color baseCol = buttonRenderer.material.color;
		baseColour = new Color(baseCol.r, baseCol.g, baseCol.b);
		brightColour = new Color(baseCol.r + 0.2f, baseCol.g + 0.2f, baseCol.b + 0.2f);
	}
	
	public abstract void pressButton();
	
	public void beginButtonHover(){
		if(buttonRenderer == null || buttonRenderer.material == null)
			return;
		buttonRenderer.material.color = brightColour;
	}
	
	public void endButtonHover(){
		buttonRenderer.material.color = baseColour;
	}
	
}
