﻿using UnityEngine;
using System.Collections;

public class HoverMenuManager : MonoBehaviour {
	
	private Renderer tooltipBox;
	private Renderer tooltipText;
	private TextMesh tooltipTextMesh;
	
	private Renderer riddleBox;
	private Renderer[] riddleBoxRenderers;
	private TextMesh riddleTextMesh;
	private TextMesh answerTextMesh;
	
	private Renderer hoverMenuBox;
	private Renderer[] hoverMenuRenderers;
	
	private ToggleScript menuSelectMode;
	
	private Rigidbody body;
	private CharacterMove movementScript;
	
	private int prevTooltipSize;
	private Color prevTooltipColour;
	
	void Start(){
		tooltipBox = GameObject.Find("TooltipBox").GetComponent<Renderer>();
		GameObject tText = GameObject.Find("TooltipText");
		tooltipText = tText.GetComponent<Renderer>();
		tooltipTextMesh = tText.GetComponent<TextMesh>();
		
		prevTooltipSize = tooltipTextMesh.fontSize;
		prevTooltipColour = tooltipTextMesh.renderer.material.color;
		
		riddleBox = GameObject.Find("RiddleBox").GetComponent<Renderer>();
		riddleBoxRenderers = riddleBox.GetComponentsInChildren<Renderer>();
		riddleTextMesh = GameObject.Find("RiddleText").GetComponent<TextMesh>();
		answerTextMesh = GameObject.Find("AnswerArea").GetComponent<TextMesh>();
		
		hoverMenuBox = GameObject.Find("HoverMenu").GetComponent<Renderer>();
		hoverMenuRenderers = hoverMenuBox.GetComponentsInChildren<Renderer>();
		
		menuSelectMode = GameObject.Find("MenuSelectMode").GetComponent<ToggleScript>();
		GameObject player = GameObject.Find("Player");
		body = player.rigidbody; // Needed to check velocity
		movementScript = player.GetComponent<CharacterMove>();
		
		hideTooltip();
		hideRiddle();
		hideMenu();
	}
	
	public void setObjectAlpha(Renderer obj, float alpha){
		Color col = obj.renderer.material.color;
		obj.renderer.material.color = new Color(col.r, col.g, col.b, alpha);
	}
	
	public void showTooltip(string tip){
		showTooltip(tip, tooltipTextMesh.fontSize, tooltipTextMesh.color);
	}
	
	public void showTooltip(string tip, int fontSize, Color col){
		if(hoverMenuBox.renderer.material.color.a > 0)
			return;
		
		prevTooltipSize = tooltipTextMesh.fontSize;
		prevTooltipColour = tooltipTextMesh.renderer.material.color;
		
		tooltipTextMesh.text = tip;
		tooltipTextMesh.fontSize = fontSize;
		tooltipTextMesh.renderer.material.color = col;
		
		setObjectAlpha(tooltipBox, 0.5f);
		setObjectAlpha(tooltipText, 1);
	}
	
	public void hideTooltip(){
		tooltipTextMesh.fontSize = prevTooltipSize;
		tooltipTextMesh.renderer.material.color = prevTooltipColour;
		
		setObjectAlpha(tooltipBox, 0);
		setObjectAlpha(tooltipText, 0);
	}
	
	public void showRiddle(string riddle){
		if(hoverMenuBox.renderer.material.color.a > 0)
			return;
		
		movementScript.reset();
		
		setObjectAlpha(riddleBox, 0.5f);
		foreach(Renderer r in riddleBoxRenderers){
			setObjectAlpha(r, 1);
			r.gameObject.SetActive(true);
		}
		
		menuSelectMode.enable();
		riddleTextMesh.text = riddle;
		answerTextMesh.text = "";
		GlobalVars.menuOpen = true;
	}
	
	public void hideRiddle(){
		setObjectAlpha(riddleBox, 0);
		foreach(Renderer r in riddleBoxRenderers){
			setObjectAlpha(r, 0);
			r.gameObject.SetActive(false);
		}
		
		menuSelectMode.disable();
		riddleTextMesh.text = "";
		GlobalVars.menuOpen = false;
	}
	
	public void showMenu(){
		if(body.velocity.magnitude > 0.1f)
			return;
		
		hideRiddle();
		hideTooltip();
		movementScript.reset();
		
		setObjectAlpha(hoverMenuBox, 1);
		foreach(Renderer r in hoverMenuRenderers){
			setObjectAlpha(r, 1);
			r.gameObject.SetActive(true);
		}
		
		menuSelectMode.enable();
		GlobalVars.menuOpen = true;
	}

	public void hideMenu(){
		setObjectAlpha(hoverMenuBox, 0);
		foreach(Renderer r in hoverMenuRenderers){
			setObjectAlpha(r, 0);
			r.gameObject.SetActive(false);
		}
		
		menuSelectMode.disable();
		GlobalVars.menuOpen = false;
	}
	
	public void changeMenuState(){
		if(riddleBox.renderer.material.color.a > 0)
			hideRiddle();
		else if(hoverMenuBox.renderer.material.color.a > 0)
			hideMenu();
		else
			showMenu();
	}
	
}
