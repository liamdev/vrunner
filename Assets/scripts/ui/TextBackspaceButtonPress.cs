﻿using UnityEngine;
using System.Collections;

public class TextBackspaceButtonPress : MenuButtonPress{

	public TextMesh textMesh;
	
	public override void pressButton(){
		textMesh.text = textMesh.text.Substring(0, textMesh.text.Length - 1);
	}
}
