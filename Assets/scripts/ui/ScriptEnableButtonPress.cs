﻿using UnityEngine;
using System.Collections;

public class ScriptEnableButtonPress : MenuButtonPress{
	
	public ToggleScript target;
	
	public override void pressButton(){
		target.enable();	
	}
	
}
