﻿using UnityEngine;
using System.Collections;

public class TextClearButtonPress : MenuButtonPress{

	public TextMesh textMesh;
	
	public override void pressButton(){
		textMesh.text = "";
	}
}
