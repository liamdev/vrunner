﻿using UnityEngine;
using System.Collections;

public class QuitButtonPress : MenuButtonPress{
	
	public override void pressButton(){
		Application.Quit();
	}
	
}
