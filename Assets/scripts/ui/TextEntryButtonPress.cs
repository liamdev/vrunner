﻿using UnityEngine;
using System.Collections;

public class TextEntryButtonPress : MenuButtonPress{
	
	public TextMesh textMesh;
	public string character;
	
	public override void pressButton(){
		if(textMesh.text.Length < GlobalVars.riddleAnswerLengthLimit)
			textMesh.text += character[0];
	}
	
}
