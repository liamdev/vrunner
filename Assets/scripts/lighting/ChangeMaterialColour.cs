﻿using UnityEngine;
using System.Collections;

public class ChangeMaterialColour : ToggleScript{
	
	public GameObject targetObject;
	public Color targetColour;
	public float transitionTime;
	
	private Material material;
	private Color startColour;
	private float elapsedTime;
	
	public Color getColourLerp(Color current, Color target, float t){
		float r = current.r + (target.r - current.r) * t;
		float g = current.g + (target.g - current.g) * t;
		float b = current.b + (target.b - current.b) * t;
		return new Color(r, g, b);
	}
	
	public override void enable(){
		base.enable();
		material = targetObject.renderer.material;
		startColour = material.color;
		elapsedTime = 0;
	}
	
	public void Update(){
		if(isDisabled())
			return;
		
		elapsedTime += Time.deltaTime;
		
		float transition = Mathf.Clamp(elapsedTime / transitionTime, 0, 1);
		material.color = getColourLerp(startColour, targetColour, transition);
		
		if(elapsedTime >= transitionTime)
			disable();
	}
	
}
