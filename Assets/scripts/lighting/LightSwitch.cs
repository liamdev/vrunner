﻿using UnityEngine;
using System.Collections;

public class LightSwitch : ToggleScript{
	
	public Light[] lights;
	public float[] intensities;
	
	void Update(){
		if(isDisabled())
			return;
		
		for(int i = 0; i < lights.Length; ++i){
			if(lights[i].intensity > 0)
				lights[i].intensity = 0;
			else
				lights[i].intensity = intensities[i];
		}
		
		disable();
	}
}
