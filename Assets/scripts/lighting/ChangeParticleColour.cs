﻿using UnityEngine;
using System.Collections;

public class ChangeParticleColour : ToggleScript{
	
	public ParticleSystem targetParticles;
	public Color targetColour;
	public float transitionTime;
	
	private Color startColour;
	private float elapsedTime;
	
	public Color getColourLerp(Color current, Color target, float t){
		float r = current.r + (target.r - current.r) * t;
		float g = current.g + (target.g - current.g) * t;
		float b = current.b + (target.b - current.b) * t;
		return new Color(r, g, b);
	}
	
	public override void enable(){
		base.enable();
		startColour = targetParticles.startColor;
		elapsedTime = 0;
	}
	
	public void Update(){
		if(isDisabled())
			return;
		
		elapsedTime += Time.deltaTime;
		
		float transition = Mathf.Clamp(elapsedTime / transitionTime, 0, 1);
		Color col = getColourLerp(startColour, targetColour, transition);
		targetParticles.startColor = col;
		
		if(elapsedTime >= transitionTime)
			disable();
	}
	
}
