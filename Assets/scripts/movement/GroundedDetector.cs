﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(Collider))]
public class GroundedDetector : CollisionHandler{
	
	private HashSet<int> floorList;
	
	private bool grounded;
	private string floorTag;
	private int layerMask;
	
	private float lastGroundedTime;
	private const float MIN_GROUNDED_INTERVAL = 0.1f;
	
	public void Start(){
		floorList = new HashSet<int>();
		
		this.grounded = false;
		layerMask = LayerMask.NameToLayer("Everything") ^ LayerMask.NameToLayer("Player");
		
		lastGroundedTime = -1000;
	}
	
	public void setGrounded(bool state){
		this.grounded = state;
	}
	
	public bool isGrounded(){
		return grounded;
	}
	
	public string getFloorTag(){
		return floorTag;
	}
	
	public override void enterCollision(Collision collision){
		if(GroupCheck.isFloor(collision.gameObject.tag) && 
			Time.time - lastGroundedTime > MIN_GROUNDED_INTERVAL){
			lastGroundedTime = Time.time;
			grounded = true;
			floorTag = collision.gameObject.tag;
			trackFloor(collision.gameObject.GetInstanceID());
		}
	}
	
	public bool raycastToGround(){
		RaycastHit hitInfo = new RaycastHit();
		
		if(Physics.Raycast(gameObject.transform.position, new Vector3(0, -1, 0), out hitInfo, 1f, layerMask)){
			if(GroupCheck.isFloor(hitInfo.collider.gameObject.tag))
				return true;
		}
		
		Debug.DrawRay(gameObject.transform.position, new Vector3(0, -1, 0), Color.magenta);
		return false;
	}
	
	public override void stayCollision(Collision collision){
		if(GroupCheck.isFloor(collision.gameObject.tag))
			trackFloor(collision.gameObject.GetInstanceID());
	}
	
	public override void exitCollision(Collision collision){
		if(GroupCheck.isFloor(collision.gameObject.tag))
			floorList.Remove(collision.gameObject.GetInstanceID());
	}
	
	
	public bool canReachFloor(){
		return floorList.Count > 0;	
	}
	
	public void trackFloor(int id){
		floorList.Add(id);
	}
	
}
