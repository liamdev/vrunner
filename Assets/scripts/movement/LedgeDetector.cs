﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(Collider))]
public class LedgeDetector : MonoBehaviour{

	private HashSet<int> floorList;
	
	private Collider selfCollider;
	private int selfID;
	
	private Vector3 floorLocation;
	private Vector3 floorDirection;
	
	public void Start(){
		floorList = new HashSet<int>();
		
		this.selfCollider = this.gameObject.GetComponent<Collider>();	
		this.selfID = selfCollider.GetInstanceID();
	}
	
	public Vector3 getFloorLocation(){
		return floorLocation;	
	}
	
	public Vector3 getFloorDirection(){
		return floorDirection;	
	}
	
	public bool canReachFloor(){
		return floorList.Count > 0;	
	}
	
	public void trackFloor(int id){
		if(id != selfID)
			floorList.Add(id);
	}
	
	public void OnTriggerEnter(Collider collider){
		if(GroupCheck.isFloor(collider.tag)){
			trackFloor(collider.GetInstanceID());
			floorLocation = collider.transform.position;
			floorDirection = collider.transform.eulerAngles;
		}
	}
	
	public void OnTriggerStay(Collider collider){
		if(GroupCheck.isFloor(collider.tag))
			trackFloor(collider.GetInstanceID());
	}
	
	public void OnTriggerExit(Collider collider){
		if(GroupCheck.isFloor(collider.tag))
			floorList.Remove(collider.GetInstanceID());
	}
	
}
