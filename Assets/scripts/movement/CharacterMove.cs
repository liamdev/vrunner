﻿ using UnityEngine;
using System.Collections;

// Need a RigidBody, or the behaviour is undefined.
[RequireComponent (typeof (Rigidbody))]
public class CharacterMove: ToggleScript{
	
	// CONSTANTS
	private const float JUMP_TOLERANCE_SEC = 0.1f; // Allow jumping if button pressed just before hitting ground
	private const float DEG_TO_RAD = Mathf.PI / 180.0f;
	
	// ===== Custom parameters =====
	// Camera
	public Camera playerCamera;
	//Movement
	public float moveForce;
	public float sidewaysSpeedModifier;
	public float backwardsSpeedModifier;
	public float maxVelocity;
	// Jumping
	public float jumpForce;
	// Wal0l running
	public float sideWallRunForceUp;
	public float frontWallRunForceUp;
	public float minWallRunSpeed;
	public float minVerticalWallRunSpeed;
	public float maxSideWallRunDuration;
	public float maxFrontWallRunDuration;
	// Transform
	public MouseTurn bodyRotationScript;
	// Animation
	public Animator animator;
	// Sounds
	public RandomAudioClip footstepPlayer;
	public EnvironmentAudio environmentAudio;
	
	// Associated body
	private Rigidbody body;
	
	// Used to manage jump state.
	public GroundedDetector groundedDetector; // Jump detectors
	public float springFloorMultiplier;
	private bool jumping; // State
	private bool groundedLastTick;
	private float lastGroundedTime;
	private float lastJumpRequest; // Time of last jump button press
	
	// Used to manage wall run state.
	public WallRunDetector[] wallRunDetectors;
	private int detectorIndex;
	private Vector3 currentWall; // Track the wall the player is currently running on, by normal direction
	private bool wallRunning;
	private bool wallRunEnabled; // Used to temporarily disable wall running in mid-air once one run has been performed.
	private bool wallJumpEnabled;
	private float wallRunStartTime;
	private float wallRunDirModifier; // Direction modifier. Running on right walls makes this 1, left walls make it -1.
	private float lastWallJumpRequest; // Time of last wall jump
	private float wallRunFootstepTime;
	
	// Used to manage climbing a ledge
	public float climbSpeed;
	public LedgeDetector ledgeDetector;
	private bool climbingLedge;
	private bool climbStart;
	private float climbDuration;
	private Vector2 climbDirection;
	
	// Used to link animation to sound events
	private float prevFootstepVal;
	
	public void Start(){
		body = gameObject.GetComponent<Rigidbody>();
		body.freezeRotation = true; // Don't want the player falling over now, do we?
		
		reset();
		
		enable();
	}
	
	public void reset(){
	lastJumpRequest = -JUMP_TOLERANCE_SEC;
		lastWallJumpRequest = -JUMP_TOLERANCE_SEC;
		jumping = false;
		groundedLastTick = true;
		
		detectorIndex = -1;
		currentWall = Vector3.zero;
		wallRunning = false;
		wallRunEnabled = true;
		wallJumpEnabled = true;
		climbingLedge = false;
		climbStart = false;
		climbDuration = 0;
		wallRunStartTime = 0;
		wallRunDirModifier = 1;
		wallRunFootstepTime = 0;
		prevFootstepVal = 2;
		
		animator.SetFloat("ForwardSpeed", 0);
		animator.SetFloat("HorizontalSpeed", 0);
		animator.SetFloat("TurnSpeed", 0);
		animator.SetBool("Grounded", true);
		animator.SetBool("Climbing", false);
		
		body.velocity = new Vector3(0, 0, 0);
	}
	
	public bool isGrounded(){
		bool groundDetected = groundedDetector.isGrounded();
		
		if(groundDetected && groundedDetector.canReachFloor()){
			groundedLastTick = true;
		} else {
			if(groundedLastTick){
				lastGroundedTime = Time.time;
				groundedLastTick = false;
			}
			
			if(Time.time - lastGroundedTime > 0.2f || !groundDetected)
				return false;
		}
		
		return true;
	}
	
	public void checkIfJumping(){
		if(isGrounded()){
			stopWallRun();
			jumping = false;
			wallRunEnabled = true;
			wallJumpEnabled = true;
		} else {
			jumping = true;	
		}
	}
	
	public void stopWallRun(){
		wallRunning = false;
		wallRunEnabled = false;
		wallJumpEnabled = false;
		bodyRotationScript.enable();	
	}
	
	public void checkSounds(){
		float footstepVal = animator.GetFloat("Footstep");
		
		if(footstepVal > 1 && prevFootstepVal < 1 && (!jumping || wallRunning))
			footstepPlayer.play();
		
		prevFootstepVal = footstepVal;
	}
	
	public Vector3 getDirectionComponents(){
		float xComponent = Mathf.Sin(playerCamera.transform.eulerAngles.y * DEG_TO_RAD);
		float yComponent = Mathf.Sin(playerCamera.transform.eulerAngles.x * -DEG_TO_RAD);
		float zComponent = Mathf.Cos(playerCamera.transform.eulerAngles.y * DEG_TO_RAD);
		return new Vector3(xComponent, yComponent, zComponent);
	}
	
	// ==================================
	// ========== FRAME UPDATE ==========
	// ==================================
	public void Update(){
		if(isDisabled() || GlobalVars.menuOpen)
			return;
		
		checkIfJumping();
		
		if(Input.GetKeyDown(KeyCode.Space))
			lastJumpRequest = Time.time;
		if(Input.GetKeyDown(KeyCode.E))
			lastWallJumpRequest = Time.time;
		
		// JUMP JUMP JUMP JUMP JUMP
		if(Time.time - lastJumpRequest < JUMP_TOLERANCE_SEC && !jumping){
			float upForce = jumpForce;
			if(groundedDetector.getFloorTag() == "SpringFloor"){
				upForce *= springFloorMultiplier;	
				environmentAudio.playSpringJump();
			}
			
			body.AddForce(0, upForce, 0);
			groundedDetector.setGrounded(false);
			jumping = true;
		}
		
		// CLIMBER? I HARDLY KNEW 'ER.
		if(climbingLedge || (jumping && !wallRunning && ledgeDetector.canReachFloor())){
			bodyRotationScript.disable();
			climbingLedge = true;
			climbDuration += Time.deltaTime;
			
			if(!climbStart){
				float angle = Mathematics.getClosestRightAngle(bodyRotationScript.transform.eulerAngles,
																ledgeDetector.getFloorDirection());
				angle *= DEG_TO_RAD;
				climbDirection = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
				climbStart = true;
			}
			
			body.velocity = new Vector3(0, 0, 0);
			
			float currentClimbState = animator.GetFloat("ClimbState");
			
			body.transform.Translate(animator.deltaPosition);
			
			if(currentClimbState < 0.9f){
				body.transform.Translate(climbDirection.x * 0.2f * Time.deltaTime,
										-0.2f * Time.deltaTime,
										climbDirection.y * 0.2f * Time.deltaTime);
			} else if(currentClimbState < 1.0f){
				body.transform.Translate(climbDirection.x * 2.5f * Time.deltaTime,
										0,
										climbDirection.y * 2.5f * Time.deltaTime);
			} else if(currentClimbState >= 1){
				climbingLedge = false;
				climbStart = false;
				climbDuration = 0;
				bodyRotationScript.enable();
				return;
			}
		}
		
		animator.SetBool("Grounded", groundedDetector.isGrounded() && !wallRunning && !jumping);
		animator.SetBool("Climbing", climbingLedge);
		animator.SetBool("Stationary", body.velocity.magnitude < 0.01f);
	}
	
	// ==================================
	// ========== FIXED UPDATE ==========
	// ==================================
	public void FixedUpdate(){
		if(isDisabled() || GlobalVars.menuOpen)
			return;
		
		checkSounds();
		
		if(climbingLedge)
			return;
		
		// RUNNIN' ON THE WALLZ
		if(jumping){
			if(Input.GetKey(KeyCode.Space)){
				// Start a wall run
				if(!wallRunning && wallRunEnabled){
					bool nearWall = false;
					for(int i = 0; i < wallRunDetectors.Length; ++i){
						if(wallRunDetectors[i].isBesideWall()){
							nearWall = true;
							currentWall = wallRunDetectors[i].getWallRunDirection();
							detectorIndex = i;
							break;
						}
					}
		
					if(nearWall){
						Vector2 charSpeed = new Vector2(body.velocity.x, body.velocity.z);
						if(charSpeed.magnitude >= minWallRunSpeed &&
							(wallRunDetectors[detectorIndex].type == WallRunDetector.WallRunType.SIDE_RUN || 	// Ensures that the minimum y velocity
							body.velocity.y > minVerticalWallRunSpeed)){										// is met for vertical wall runs.
							wallRunning = true;
							wallRunStartTime = Time.time;
							if(wallRunDetectors[detectorIndex].type == WallRunDetector.WallRunType.SIDE_RUN){ // WALL TO THE SIDE
								// Left walls need to flip the wall run direction. Check if the wall is on the left or right.
								Vector2 wallDir = new Vector2(currentWall.x, currentWall.z);
								wallRunDirModifier = (Vector2.Dot(charSpeed, wallDir) >= 0) ? 1 : -1;
							}
							bodyRotationScript.disable();
						}
					}
				}

				// Maintain a wall run
				if(wallRunning){
					if(Time.time - lastWallJumpRequest < JUMP_TOLERANCE_SEC && wallJumpEnabled){ // JUMP OFF WALL
						Vector3 dir = getDirectionComponents();
						body.velocity = new Vector3(0, 0, 0);
						
						float pushForce = jumpForce;
						if(wallRunDetectors[detectorIndex].getWallTag() == "SpringWall"){
							pushForce *= 2;
							environmentAudio.playSpringJump();
						}
						body.AddForce(pushForce * dir.x, pushForce * Mathf.Clamp(dir.y, 0.5f, 1f), pushForce * dir.z);
						stopWallRun();
					} else { // TRY TO KEEP RUNNING
						
						// Sticky walls let you keep wall running
						if(wallRunDetectors[detectorIndex].getWallTag() == "StickyWall")
							wallRunStartTime = Time.time;
						
						float timeDiff = Time.time - wallRunStartTime;
						float maxTime = (wallRunDetectors[detectorIndex].type == WallRunDetector.WallRunType.SIDE_RUN) ?
										maxSideWallRunDuration :
										maxFrontWallRunDuration;
						if(wallRunDetectors[detectorIndex].isBesideWall()
							&& wallRunDetectors[detectorIndex].getWallRunDirection() == currentWall){
							
							wallRunFootstepTime += Time.fixedDeltaTime;
							if(wallRunFootstepTime >= 0.2f){
								footstepPlayer.play();
								wallRunFootstepTime -= 0.2f;
							}
							
							if(wallRunDetectors[detectorIndex].type == WallRunDetector.WallRunType.SIDE_RUN){ // SIDE RUN
								float durationProgress = Mathf.Clamp((timeDiff / maxTime), 0, 1);
								float upForce = sideWallRunForceUp * Mathf.Cos(durationProgress * (Mathf.PI / 2f));
								body.AddForce(0, upForce, 0);
								Vector2 scaledSpeed = new Vector2(currentWall.x * maxVelocity, currentWall.z * maxVelocity) * wallRunDirModifier;
								float yVel = (wallRunDetectors[detectorIndex].getWallTag() == "StickyWall") ?
													0 : body.velocity.y;
								body.velocity = new Vector3(scaledSpeed.x, yVel, scaledSpeed.y);
							} else { //FRONT RUN
								body.AddForce(0, frontWallRunForceUp * 2.5f * Mathf.Cos((timeDiff / maxTime) * (Mathf.PI / 2f)), 0);
								body.velocity = new Vector3(0, Mathf.Clamp(body.velocity.y, 0, maxVelocity), 0);
								if(ledgeDetector.canReachFloor()){
									climbingLedge = true;
									stopWallRun();
								} else if(timeDiff > maxTime){
									stopWallRun();
								}
									
							}
						} else { // Still 'wall running', but no longer near a wall, or the run has reached max duration
							stopWallRun();
						}
					}
				}
			} else if(wallRunning){ // Still 'wall running', but no longer holding space
				stopWallRun();
			}
		}
		
		// MOVE IT, FOOTBALL-HEAD.
		if(!jumping){
			float yRot = bodyRotationScript.getTarget().transform.eulerAngles.y * DEG_TO_RAD;
			
			float zComponent = Mathf.Cos(yRot);
			float xComponent = Mathf.Sin(yRot);
			
			float forwardInput 	= Input.GetAxis("Vertical");
			float strafeInput	= Input.GetAxis("Horizontal");
			
			Vector2 inputDir = new Vector2(strafeInput, forwardInput);
			inputDir.Normalize();
			
			animator.SetFloat("ForwardSpeed", forwardInput, 0.1f, Time.fixedDeltaTime);
			animator.SetFloat("HorizontalSpeed", strafeInput, 0.1f, Time.fixedDeltaTime);
			animator.SetFloat("TurnSpeed", bodyRotationScript.getTurnVelocity(), 0.1f, Time.fixedDeltaTime);
			
			inputDir.x *= sidewaysSpeedModifier; // Sideways movement is slower.
			if(inputDir.y < 0)
				inputDir.y *= backwardsSpeedModifier; // Backwards movement is slower.
			
			float zForce = zComponent * inputDir.y + xComponent * -inputDir.x;
			float xForce = zComponent * inputDir.x + xComponent * inputDir.y;
			
			body.AddForce(xForce * moveForce, 0, zForce * moveForce);
			
			Vector2 clampedSpeed = Vector2.ClampMagnitude(new Vector2(body.velocity.x, body.velocity.z), maxVelocity);
			body.velocity = new Vector3(clampedSpeed.x, body.velocity.y, clampedSpeed.y);
		}
	}
}
