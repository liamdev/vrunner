﻿using UnityEngine;
using System.Collections;

public class MouseTurn : ToggleScript {
	
	public GameObject[] targets;
	public float sensitivity = 10;
	
	private float turnVelocity;
	
	public GameObject getTarget(){
		return gameObject;
	}
	
	void Start(){
		enable();
	}
	
	public float getTurnVelocity(){
		return turnVelocity;
	}
	
	void Update(){
		if(isDisabled())
			return;
		
		turnVelocity = Input.GetAxis("Mouse X") * sensitivity;
		
		// ALL YOU HAD TO DO WAS FOLLOW THE DAMN MOUSE, CJ.
		foreach(GameObject go in targets){
			go.transform.Rotate(0,
								turnVelocity,
								0);
		}
	}
}
