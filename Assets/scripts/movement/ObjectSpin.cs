﻿using UnityEngine;
using System.Collections;

public class ObjectSpin : MonoBehaviour {

	public float spinDuration;
	
	public void Update(){
		if(spinDuration == 0)
			return;
		
		gameObject.transform.Rotate(0, (Time.deltaTime / spinDuration) * 360, 0);
	}
}