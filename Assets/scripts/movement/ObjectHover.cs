﻿using UnityEngine;
using System.Collections;

public class ObjectHover : MonoBehaviour {

	public float bobDuration;
	public float bobHeight;
	
	private Vector3 startPosition;
	private float elapsedTime;
	
	public void Start(){
		startPosition = gameObject.transform.position;
		elapsedTime = 0;
	}
	
	public void Update(){
		if(bobDuration == 0)
			return;
		
		elapsedTime += Time.deltaTime;
		
		while(elapsedTime > bobDuration)
			elapsedTime -= bobDuration;
		
		Vector3 currentLocation = this.transform.position;
		float yOffset = Mathf.Cos((elapsedTime / bobDuration) * 2 * Mathf.PI) * bobHeight;
		currentLocation.y = startPosition.y + yOffset;
		
		this.transform.position = currentLocation;
	}
}
