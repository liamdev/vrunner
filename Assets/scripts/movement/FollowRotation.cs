﻿using UnityEngine;
using System.Collections;

public class FollowRotation : ToggleScript{
	
	// Ensure this is not a child of the attached GameObject,
	// or the transform inheritance hierarchy causes a rotation loop,
	// where the two objects are trying to copy each other's rotations.
	public GameObject target;
	
	public bool followX = false;
	public bool followY = false;
	public bool followZ = false;
	
	void Start(){
		enable();	
	}
	
	void Update(){
		if(isDisabled())
			return;
		
		float x = (followX) ? target.transform.eulerAngles.x : transform.eulerAngles.x;
		float y = (followY) ? target.transform.eulerAngles.y : transform.eulerAngles.y;
		float z = (followZ) ? target.transform.eulerAngles.z : transform.eulerAngles.z;
		
		transform.eulerAngles = new Vector3(x, y, z);
	}
}
