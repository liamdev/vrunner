﻿using UnityEngine;
using System.Collections;

public static class GroupCheck{
	
	public static bool isFloor(string tag){
		return (tag == "Floor" || tag == "SpringFloor");	
	}
	
	public static bool isWall(string tag){
		return (tag == "Wall" || tag == "SpringWall" || tag == "StickyWall");
	}

}
