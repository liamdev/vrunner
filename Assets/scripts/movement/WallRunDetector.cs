﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(Collider))]
public class WallRunDetector : MonoBehaviour{
	
	public enum WallRunType {SIDE_RUN, FRONT_RUN};
	public WallRunType type;
	
	private HashSet<int> wallList;
	
	private Collider selfCollider;
	private int selfID;
	
	private Vector3 wallRunDir;
	private string wallTag;
	
	public void Start(){
		wallList = new HashSet<int>();
		
		this.selfCollider = this.gameObject.GetComponent<Collider>();	
		this.selfID = selfCollider.GetInstanceID();
	}
	
	public bool isBesideWall(){
		return wallList.Count > 0;	
	}
	
	public Vector3 getWallRunDirection(){
		return wallRunDir;	
	}
	
	public void trackWall(int id){
		if(id != selfID)
			wallList.Add(id);
	}
	
	public string getWallTag(){
		return wallTag;
	}
	
	public void OnTriggerEnter(Collider collider){
		if(GroupCheck.isWall(collider.tag)){
			wallRunDir = (type == WallRunType.SIDE_RUN) ? collider.transform.forward : collider.transform.right;
			wallTag = collider.tag;
			trackWall(collider.GetInstanceID());
		}
	}
	
	public void OnTriggerStay(Collider collider){
		if(GroupCheck.isWall(collider.tag))
			trackWall(collider.GetInstanceID());
	}
	
	public void OnTriggerExit(Collider collider){
		if(GroupCheck.isWall(collider.tag))
			wallList.Remove(collider.GetInstanceID());
	}
	
}
