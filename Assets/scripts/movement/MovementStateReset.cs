﻿using UnityEngine;
using System.Collections;

public class MovementStateReset : ToggleScript{
	
	public CharacterMove movementScript;
	
	public void Update(){
		if(isDisabled())
			return;
		
		movementScript.reset();
		
		disable();
	}
	
}
