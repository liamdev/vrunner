﻿using UnityEngine;
using System.Collections;

public class ToggleScript : MonoBehaviour{
	
	protected bool scriptEnabled = false;
	
	public bool isEnabled(){
		return scriptEnabled;
	}
	
	public bool isDisabled(){
		return !scriptEnabled;
	}
	
	public virtual void enable(){
		scriptEnabled = true;
	}
	
	public virtual void disable(){
		scriptEnabled = false;	
	}
	
}
