﻿using UnityEngine;
using System.Collections;

public class CollisionSwitch : MonoBehaviour {
	
	public GameObject[] collisionObjects;
	
	// The index of a given key must be equal to its target handler script.
	private Collider[] colliderKeys;
	private CollisionHandler[] handlerScripts;
	
	public void Start(){
		colliderKeys = new Collider[collisionObjects.Length];
		handlerScripts = new CollisionHandler[collisionObjects.Length];
		
		for(int i = 0; i < collisionObjects.Length; ++i){
			colliderKeys[i] = collisionObjects[i].GetComponent<Collider>();
			handlerScripts[i] = collisionObjects[i].GetComponent<CollisionHandler>();
		}
	}
	
	private int findColliderIndex(Collision collision){
		if(collision.contacts.Length < 0)
			return -1;
		
		for(int i = 0; i < colliderKeys.Length; ++i){
			if(	collision.contacts[0].thisCollider	== colliderKeys[i] ||
				collision.contacts[0].otherCollider	== colliderKeys[i])
				return i;
		}
		return -1;
	}
	
	public void OnCollisionEnter(Collision collision){
		int index = findColliderIndex(collision);
		if(index >= 0)
			handlerScripts[index].enterCollision(collision);
	}
	
	void OnCollisionStay(Collision collision){
		int index = findColliderIndex(collision);
		if(index >= 0)
			handlerScripts[index].stayCollision(collision);
	}
	
	void OnCollisionExit(Collision collision){
		int index = findColliderIndex(collision);
		if(index >= 0)
			handlerScripts[index].exitCollision(collision);
	}
}
