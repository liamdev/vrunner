﻿using UnityEngine;
using System.Collections;

public class ParallelScripts : ToggleScript {
	
	public ToggleScript[] scripts;
	
	private bool lastState;
	
	void Update(){
		if(isDisabled()){
			lastState = false;
			return;
		}
		
		if(lastState == false){
			foreach(ToggleScript script in scripts){
				script.enable();
			}
			lastState = true;
		}
		
		bool finished = true;
		foreach(ToggleScript script in scripts){
			finished = finished && script.isDisabled();
		}
		
		if(finished){
			disable();
			lastState = false;
		}
	}
}
