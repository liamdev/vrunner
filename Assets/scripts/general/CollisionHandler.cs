﻿using UnityEngine;
using System.Collections;

public abstract class CollisionHandler : MonoBehaviour {
	
	public abstract void enterCollision(Collision collision); 	// Called on start of collision.
	public abstract void stayCollision(Collision collision);	// Called each frame in which the collision is ongoing.
	public abstract void exitCollision(Collision collision);	// Called on end of collision.
	
}
