﻿using UnityEngine;
using System.Collections;

public class MouseHide : MonoBehaviour{

	void Start(){
		Screen.showCursor = false;
		Screen.lockCursor = true;
	}
	
}
