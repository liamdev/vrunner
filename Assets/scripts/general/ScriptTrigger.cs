﻿using UnityEngine;
using System.Collections;

public class ScriptTrigger : MonoBehaviour {
	
	public ToggleScript targetScript;
	public float delay = 0;
	
	private bool triggered;
	private bool finished;
	private float elapsedTime;
	
	public void Start(){
		triggered = false;
		finished = false;
		elapsedTime = 0;
	}
	
	public void OnTriggerEnter(){
		triggered = true;
	}
	
	public void Update(){
		if(finished || !triggered)
			return;
		
		elapsedTime += Time.deltaTime;
		
		if(elapsedTime > delay){
			targetScript.enable();
			finished = true;	
		}
	}
	
}
