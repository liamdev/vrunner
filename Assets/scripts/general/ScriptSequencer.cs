﻿using UnityEngine;
using System.Collections;

public class ScriptSequencer : ToggleScript{
	
	public ToggleScript[] sequence;
	public bool runImmediately = true;
	
	private int currentScript;
	
	void Start(){
		if(runImmediately){
			enable();
		} else {
			currentScript = 0;
			disable();
		}
	}
	
	public override void enable(){
		base.enable();
		currentScript = 0;
		
		if(sequence.Length < 1)
			return;
		
		for(int i = 0; i < sequence.Length; ++i)
			sequence[i].disable();
		
		sequence[currentScript].enable();
	}
	
	void Update(){
		if(isDisabled())
			return;
		
		if(currentScript < sequence.Length){
			if(!sequence[currentScript].isEnabled()){
				++currentScript;
				if(currentScript < sequence.Length){
					sequence[currentScript].enable();
				}
			}
		} else {
			disable();
		}
	}
}
