using System;
using UnityEngine;

public class Mathematics{
	
	// This is horrific, but I can't be bothered to work out a better way
	public static float getClosestRightAngle(Vector3 selfDirection, Vector3 otherDirection){
		float selfAngle = selfDirection.y % 360;
		float otherAngle = otherDirection.y;
		
		float angle1 = otherAngle % 360;
		float angle2 = (otherAngle + 90) % 360;
		float angle3 = (otherAngle + 180) % 360;
		float angle4 = (otherAngle + 270) % 360;
		float angle5 = Mathf.Min(Mathf.Min(angle1, angle2), Mathf.Min(angle3, angle4)) - 90;
		float angle6 = Mathf.Max(Mathf.Max(angle1, angle2), Mathf.Max(angle3, angle4)) + 90;
		
		float diff1 = Mathf.Abs(angle1 - selfAngle);
		float diff2 = Mathf.Abs(angle2 - selfAngle);
		float diff3 = Mathf.Abs(angle3 - selfAngle);
		float diff4 = Mathf.Abs(angle4 - selfAngle);
		float diff5 = Mathf.Abs(angle5 - selfAngle);
		float diff6 = Mathf.Abs(angle6 - selfAngle);
		
		if(diff1 < diff2 && diff1 < diff3 && diff1 < diff4 && diff1 < diff5 && diff1 < diff6)
			return angle1;
		else if(diff2 < diff3 && diff2 < diff4 && diff2 < diff5 && diff2 < diff6)
			return angle2;
		else if(diff3 < diff4 && diff3 < diff5 && diff3 < diff6)
			return angle3;
		else if(diff4 < diff5 && diff4 < diff6)
			return angle4;
		else if(diff5 < diff6)
			return angle5 + 360;
		else
			return angle6 - 360;
	}
	
}

