﻿using UnityEngine;
using System.Collections;

public class ScriptSwitch : ToggleScript{
	
	public ToggleScript[] targets;
	
	// Flick the switch on the target scripts, then disable self.
	void Update(){
		if(isDisabled())
			return;
		
		foreach(ToggleScript target in targets){
			if(target.isEnabled())
				target.disable();
			else
				target.enable();
		}
		
		disable();
	}
}
