﻿using UnityEngine;
using System.Collections;

public class FindAndEnable : ToggleScript{
	
	public string scriptFindName;
	
	private ToggleScript scriptTarget;
	
	public void Start(){
		scriptTarget = GameObject.Find(scriptFindName).GetComponent<ToggleScript>();
	}
	
	public void Update(){
		if(isDisabled())
			return;
		
		scriptTarget.enable();
		
		disable();
	}
	
}
