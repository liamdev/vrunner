﻿using UnityEngine;
using System.Collections;

public static class GlobalVars : System.Object{
	
	public static bool inLevel = false;
	public static bool menuOpen = false;
	
	public static int riddleAnswerLengthLimit = 12;
}
