﻿using UnityEngine;
using System.Collections;

public class DelayScript : ToggleScript{
	
	public float duration;
	
	private float elapsedTime;
	
	public override void enable(){
		base.enable();
		elapsedTime = 0;	
	}
	
	public void Update(){
		if(isDisabled())
			return;
		
		elapsedTime += Time.deltaTime;
		
		if(elapsedTime >= duration)
			disable();
	}
}
