﻿using UnityEngine;
using System.Collections;

public class CollisionMessenger : MonoBehaviour{
	
	public CollisionHandler recipient;
	
	public void OnCollisionEnter(Collision collision){
		recipient.enterCollision(collision);
	}
	
	void OnCollisionStay(Collision collision){
		recipient.stayCollision(collision);
	}
	
	void OnCollisionExit(Collision collision){
		recipient.exitCollision(collision);
	}
}
