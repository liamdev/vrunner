﻿using UnityEngine;
using System.Collections;

public class ButtonPress : MonoBehaviour{
	
	public KeyCode exitKey = KeyCode.Escape;
	public KeyCode debugBreakKey = KeyCode.F1;
	
	private HoverMenuManager menuManager;
	
	private GameObject hoverMenuBox;
	
	public void Start(){
		menuManager = GameObject.Find("HoverMenuManager").GetComponent<HoverMenuManager>();
	}
	
	void exitGame(){
		Application.Quit();	
	}
	
	void Update(){
		if(Input.GetKeyDown(exitKey)){
			if(GlobalVars.inLevel){
				menuManager.changeMenuState();
			} else {
				exitGame();
			}
		}
		if(Input.GetKeyDown(debugBreakKey) && Application.isEditor)
			Debug.Break();
	}
	
}
