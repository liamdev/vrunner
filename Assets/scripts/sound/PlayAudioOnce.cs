﻿using UnityEngine;
using System.Collections;

public class PlayAudioOnce : ToggleScript{
	
	public AudioSource source;
	
	public void Update(){ 
		if(isDisabled())
			return;
	
		source.Play();
		
		disable();
	}
}
