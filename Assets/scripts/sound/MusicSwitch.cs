﻿using UnityEngine;
using System.Collections;

public class MusicSwitch : ToggleScript{
	
	private MusicPlayer player;
	
	public void Start(){
		player = GameObject.Find("MusicPlayer").GetComponent<MusicPlayer>();
	}
	
	public override void enable(){
		base.enable();
	
		if(player.isPlaying())
			player.stop();
		else
			player.play();
		
		disable();
	}
	
}
