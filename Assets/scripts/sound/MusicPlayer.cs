﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class MusicPlayer : MonoBehaviour{
	
	private AudioSource player;
	
	private ArrayList trackList;
	private int trackIndex;
	
	private const float FADE_DURATION = 2.0f;
	private const float DEFAULT_VOL_LEVEL = 0.4f;
	private float fadeStartTime;
	private bool fadingOut;
	
	public void Start(){
		player = gameObject.audio;
		trackList = new ArrayList();
		trackIndex = 0;
		fadingOut = false;
	}
	
	public void playTrack(AudioClip track){
		player.clip = track;
		player.volume = DEFAULT_VOL_LEVEL;
		player.Play();
	}
	
	public void queueTrack(AudioClip track){
		trackList.Add(track);
	}
	
	public void play(){
		nextTrack();
	}
	
	public void stop(){
		fadingOut = true;
		fadeStartTime = Time.time;
	}
	
	public bool isPlaying(){
		return (player.clip != null && !fadingOut);
	}
	
	public void nextTrack(){
		if(trackList.Count < 1)
			return;
		
		trackIndex = (trackIndex + 1) % trackList.Count;
		player.clip = (AudioClip)trackList[trackIndex];
		player.volume = DEFAULT_VOL_LEVEL;
		player.Play();
	}
	
	public void Update(){
		if(fadingOut){
			float timeDiff = Time.time - fadeStartTime;
			if(timeDiff > FADE_DURATION){
				player.volume = 0;
				player.clip = null;
				fadingOut = false;
			} else {
				float interpVal = timeDiff / FADE_DURATION;
				player.volume = DEFAULT_VOL_LEVEL - (interpVal * DEFAULT_VOL_LEVEL);
			}
		}
		
		if(!player.isPlaying && player.clip != null)
			nextTrack();
	}
	
}
