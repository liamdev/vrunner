﻿using UnityEngine;
using System.Collections;

public class EnvironmentAudio : MonoBehaviour {
	
	public AudioSource source;
	
	public AudioClip springJumpSound;
	
	public void playSpringJump(){
		source.PlayOneShot(springJumpSound);	
	}
	
}
