﻿using UnityEngine;
using System.Collections;

public class PlayTrack : ToggleScript{
	
	public AudioClip clip;
	
	private MusicPlayer player;
	
	public void Start(){
		player = GameObject.Find("MusicPlayer").GetComponent<MusicPlayer>();
	}
	
	public override void enable(){
		base.enable();
		
		player.playTrack(clip);
		
		disable();
	}
	
}
