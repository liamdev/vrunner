﻿using UnityEngine;
using System.Collections;

public class MusicQueueFiller : ToggleScript{
	
	public AudioClip[] tracks;
	
	private MusicPlayer player;
	
	void Start(){
		player = GameObject.Find("MusicPlayer").GetComponent<MusicPlayer>();
		enable();
	}
	
	void Update(){
		if(isDisabled())
			return;
		
		foreach(AudioClip clip in tracks)
			player.queueTrack(clip);
		
		player.stop();
		
		disable();
	}
}
