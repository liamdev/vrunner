﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class RandomAudioClip : MonoBehaviour {
	
	public AudioClip[] clips;
	private AudioSource source;
	
	public void Start(){
		source = gameObject.GetComponent<AudioSource>();
	}
	
	public void play(){
		if(clips.Length < 1)
			return;
		
		source.PlayOneShot(clips[Random.Range(0, clips.Length)]);
	}
	
}
