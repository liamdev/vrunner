﻿// Slightly altered shader, original: http://wiki.unity3d.com/index.php?title=3DText

Shader "Custom/font-shader"{

	Properties {
		_MainTex ("Font Texture", 2D) = "white" {}
		_Color ("Font Colour", Color) = (1, 1, 1, 1)
	}
	
	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="False" "RenderType"="Transparent"}
		
		Lighting Off Cull Off ZWrite On Fog {Mode Off}
		
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass{
			Color [_Color]
			SetTexture [_MainTex]{
				combine primary, texture * primary
			}
		}
	}
	
	FallBack "Diffuse"
}
